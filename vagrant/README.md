Start the vagrant box with Virtualbox:
```
vagrant up
```

Log in:
```
vagrant ssh
```

Build *Core GNOME UX Shell*:
```
jhbuild build meta-gnome-core-shell mutter
```

Run gnome-shell:
```
jhbuild run gnome-shell
```
