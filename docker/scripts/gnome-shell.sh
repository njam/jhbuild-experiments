#!/usr/bin/env bash

set -e

export XDG_RUNTIME_DIR=/run/user/$(id -u)
export JHBUILD_RUN_AS_ROOT=1
export MUTTER_VERBOSE=1
export MUTTER_DEBUG=1

# FIXME: Workaround for missing file "/root/jhbuild/install/bin/Xwayland"
# This should be installed by JHBuild - why isn't it?
if ! (test -e /root/jhbuild/install/bin/Xwayland); then
  ln -s /usr/bin/Xwayland /root/jhbuild/install/bin/Xwayland
fi

# Run gnome-shell in a new D-Bus session
exec dbus-run-session -- jhbuild run gnome-shell --nested --wayland
