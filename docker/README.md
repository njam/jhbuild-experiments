JHBuild in a Docker container
=============================

This Docker image has [JHBuild](https://wiki.gnome.org/Projects/Jhbuild) installed, as well as
system dependencies to build `gnome-shell`.

The sub directory `jhbuild/` will be bind-mounted into the container. It contains the configuration
file `jhbuildrc` and JHBuild's *checkout*, *install* and *cache* directories.
This allows one to edit a package's source code in the *checkout* directory, and then build that package
inside the container.

All commands should be executed in a checkout of this repository.
So first clone the repo, then change into its `docker/` subdirectory:
```
git clone https://gitlab.com/njam/jhbuild-experiments.git
cd jhbuild-experiments/docker/
```

Build the Docker image:
```sh
docker build -t jhbuild-experiments:latest .
```

Test if `jhbuild` runs in the container:
```sh
docker run -it --rm \
  -v $(pwd)/jhbuild:/root/jhbuild \
  jhbuild-experiments:latest \
  jhbuild help
```


Build & Run gnome-shell
-----------------------

Build *Core GNOME UX Shell* (this can take multiple hours on the first run):
```sh
docker run -it --rm \
  -v $(pwd)/jhbuild:/root/jhbuild \
  jhbuild-experiments:latest \
  jhbuild build meta-gnome-core-shell mutter
```

To run `gnome-shell` we need systemd running in the container, as well as an X server outside to
connect to in *nested* mode (nested mode within Wayland is not possible). 

Allow to connect to your X server:
```sh
xhost +local:
```

Start systemd in a container:
```sh
docker run -it --rm \
  -v $(pwd)/jhbuild:/root/jhbuild \
  \
  --tmpfs /run \
  --tmpfs /run/lock \
  --volume /sys/fs/cgroup:/sys/fs/cgroup:ro \
  --stop-signal 'SIGRTMIN+3' \
  --env 'container=docker' \
  \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v /dev/dri/card0:/dev/dri/card0 \
  -v /dev/dri/renderD128:/dev/dri/renderD128 \
  \
  jhbuild-experiments:latest \
  /bin/bash -c 'exec /sbin/init --log-target=journal 3>&1'
```

Then in a second terminal start `gnome-shell` with `systemd-run`:
```
docker exec -it $(docker ps -qf ancestor=jhbuild-experiments) \
  systemd-run --scope -E DISPLAY=$DISPLAY gnome-shell.sh
```
![](doc/gnome-shell-nested.png)


Build & Run Gedit
-----------------

Build *Gedit*:
```sh
docker run -it --rm \
  -v $(pwd)/jhbuild:/root/jhbuild \
  jhbuild-experiments:latest \
  jhbuild build gedit
```

Single applications can be rendered within the host's Wayland compositor.
For that to work the wayland socket is bind-mounted into the container, and `WAYLAND_DISPLAY` must be set accordingly.

Run Gedit by mounting the Wayland socket:
```sh
docker run -it --rm \
  -v $(pwd)/jhbuild:/root/jhbuild \
  -e XDG_RUNTIME_DIR=/tmp \
  -e WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
  -v $XDG_RUNTIME_DIR/$WAYLAND_DISPLAY:/tmp/$WAYLAND_DISPLAY \
  jhbuild-experiments:latest \
  jhbuild run gedit
```
![](doc/gedit.png)


Using `docker-machine` to run on EC2
------------------------------------

One could use an EC2 cloud instance for faster compilation time.
This doesn't work yet, what follows are just some notes. For example mounting volumes will not work from your local computer.

Provision an EC2 machine with Ubuntu 17.04 and Docker:
```sh
docker-machine create \
  --driver amazonec2 \
  --amazonec2-access-key <key> --amazonec2-secret-key <secret> \
  --amazonec2-region "eu-central-1" \
  --amazonec2-ami "ami-ec832a83" \
  --amazonec2-root-size 50 \
  --amazonec2-instance-type "c4.8xlarge" \
  jhbuild-experiments
```

Connect Docker client to remote Docker engine:
```sh
eval $(docker-machine env jhbuild-experiments)
```

Build the Docker image:
```sh
docker build -t jhbuild-experiments:latest .
```

Build *Core GNOME UX Shell*:
```sh
docker run -it --rm \
  -v /home/ubuntu/jhbuild:/root/jhbuild \
  jhbuild-experiments:latest \
  jhbuild build meta-gnome-core-shell mutter
```
