Create a Docker container and export the filesystem:
```
CONTAINER_ID=$(docker create jhbuild-experiments:latest)
docker export -o container.tar $CONTAINER_ID
docker rm $CONTAINER_ID
```

Import the container to `/var/lib/machines/`:
```
sudo machinectl import-tar container.tar jhbuild-experiments
```

Start bash:
```
sudo systemd-nspawn --machine jhbuild-experiments /bin/bash
```

Boot the machine:
```
sudo systemd-nspawn --boot \
  --machine jhbuild-experiments \
  --bind=$(pwd)/../docker/jhbuild:/root/jhbuild \
  --bind=/tmp/.X11-unix:/tmp/.X11-unix \
  --bind=/dev/dri/card0:/dev/dri/card0 \
  --bind=/dev/dri/renderD128:/dev/dri/renderD128 \
  --setenv=DISPLAY=$DISPLAY
```
